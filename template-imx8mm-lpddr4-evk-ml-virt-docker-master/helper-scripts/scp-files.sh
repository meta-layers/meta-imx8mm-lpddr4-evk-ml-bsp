#!/bin/bash

pushd /workdir/build/imx8mm-lpddr4-evk-ml-virt-docker-master/tmp/deploy/images/imx8mm-lpddr4-evk-ml

SOURCE_1="core-image-minimal-virt-docker-imx8mm-lpddr4-evk-ml*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/imx8mm-lpddr4-evk-ml-virt-docker-master/tmp/deploy/images/imx8mm-lpddr4-evk-ml"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
