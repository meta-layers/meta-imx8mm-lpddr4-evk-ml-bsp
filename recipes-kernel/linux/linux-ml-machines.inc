# Setup for arm-64-ml
COMPATIBLE_MACHINE = "^$"

COMPATIBLE_MACHINE_arm64-ml = "arm64-ml"
COMPATIBLE_MACHINE_imx8mm-lpddr4-evk-ml = "imx8mm-lpddr4-evk-ml"

# Default kernel config fragements for specific machines
#KERNEL_FEATURES_append_qemumicroblaze += "bsp/qemumicroblaze/qemumicroblaze.scc"
