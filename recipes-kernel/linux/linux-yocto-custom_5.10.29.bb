# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-5.10.y"

LINUX_VERSION = "5.10.29"

# some 5.10.x LIC_FILE_CHECKSUM:
LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2021-04-10 13:36:11 +0200
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2021-04-10 13:36:11 +0200
# commit	d8cf82b410b4be8a1266c10d05453128bd40d03a (patch)
# tree		b9e8178a6cb3bd837c3abd313afe88fd97ad3987
# parent	cef13a04376b44b71196f74b29941678c18bc7ec (diff)
# download	linux-d8cf82b410b4be8a1266c10d05453128bd40d03a.tar.gz
# Linux 5.10.29 v5.10.29

SRCREV ?= "d8cf82b410b4be8a1266c10d05453128bd40d03a"

PATCHPATH="${THISDIR}/patch/5.10.x"

FILESEXTRAPATHS_prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://arm64-ml-user-patches.scc \
           "
SRC_URI_append += " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "
